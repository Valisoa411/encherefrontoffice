import { useState } from "react";

export default function Login() {
    const [error, setError] = useState('');
    const [formData, setFormData] = useState({
        email: 'rasoa@yahoo.fr',
        mdp: 'rasoa0000'
    });

    const handleChange = event => {
        setFormData({
          ...formData,
          [event.target.name]: event.target.value
        });
      };

    const submitForm = event => {
        event.preventDefault();
        fetch("http://localhost:8080/clients/login?email="+formData.email+"&mdp="+formData.mdp, {method: "POST"})
        .then((data) => data.json())
        .then((res) => {
            if(res.error) {
                throw new Error(res.error.message);
            }
            localStorage.setItem("token", res.data.data.token);
            window.location = '/home';
        })
        .catch((error) => {
            console.log("error : " + error.message);
            setError(error.message);
        });
    }
    

    return (
        <>
            <h1>Login</h1>
            <form onSubmit={submitForm}>
                <p>Login: <input type="text" name="email" value={formData.email} onChange={handleChange}/></p>
                <p>Password: <input type="password" name="mdp" value={formData.mdp} onChange={handleChange}/></p>
                <p color="red">{error}</p>
                <p><input type="submit" value="Connection"/></p>
            </form>
        </>
    );
}