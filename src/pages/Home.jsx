import { useEffect, useState } from "react";
import EnchereList from "../components/EnchereList";
import Fiche from "../components/Fiche";

export default function Home() {
    const [encheres, setEncheres] = useState([]);
    const [selectedItem, setSelectedItem] = useState(null);
    const [token, setToken] = useState(localStorage.getItem("token"));

    const handleClick = (item) => setSelectedItem(item);
    const handleClose = () => setSelectedItem(null);
    
    const deconnection = () => {
        if(token!=null) {
            setToken(null);
            return;
        }
        fetch('http://localhost:8080/clients/logout?token='+token)
        .then((data) => data.json())
        .then((res) => {
            if(res.error) throw new Error(res.error);
            localStorage.setItem("token",null);
            setToken(null);
            window.location = '/login';
        })
        .catch((er) => {
            console.error("error : "+er.message);
        });
    }

    useEffect(() => {
        fetch('http://localhost:8080/encheres/v')
        .then(data => data.json())
        .then(res => {
            setEncheres(res.data.data.listenchere);
        })
    }, []);

    return (
        <>
            {!token ? (<a href="/login"><button>Connection</button></a>) : (<button onClick={deconnection}>Deconnection</button>)}
            <EnchereList encheres={encheres} handleClick={handleClick} />
            {selectedItem && <Fiche enchere={selectedItem} handleClose={handleClose} />}
        </>
    );
}