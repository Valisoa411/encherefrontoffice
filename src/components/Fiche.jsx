import { useEffect, useState } from 'react';
import './Fiche.css';

const Fiche = ({ enchere, handleClose }) => {
  const [inputValue,setInputValue] = useState(0);
  const [currentEnchere,setCurrentEnchere] = useState(enchere);
  const [token, setToken] = useState(localStorage.getItem("token"));

  useEffect(() => {
    setCurrentEnchere(enchere);
  }, [enchere]);

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const submitForm = async (event) => {
    event.preventDefault();
    // handleSubmit(currentEnchere.id,inputValue);
    // setInputValue(0);
    if(token==null){
      window.location="/login";
    } else {
        var link = 'http://localhost:8080/clients/rencherir/'+currentEnchere.id+"?montant="+inputValue+"&token="+token;
        await fetch(link,{method: "POST"})
        .then((data) => data.json())
        .then((res) => {
            console.log(res.error);
            if(res.error) throw new Error(res.error.message);
            console.log("message : "+res.data.message);
        })
        .catch((er) => {
            if(er.message === 'Exception : java.lang.Exception: Not Connected') deconnection();
            console.error("error : "+er.message);
        });
        await fetch('http://localhost:8080/encheres/v/'+currentEnchere.id)
        .then((data) => data.json())
        .then((res) => {
          if(res.error) throw new Error(res.error.message);
          setCurrentEnchere(res.data.data.enc);
        })
        .catch((er) => {
          if(er.message === 'Exception : java.lang.Exception: Not Connected') deconnection();
          console.error("error : "+er.message);
      });
    }
  };
  const deconnection = () => {
    if(token!=null) {
        setToken(null);
        return;
    }
    fetch('http://localhost:8080/clients/logout?token='+token)
    .then((data) => data.json())
    .then((res) => {
        if(res.error) throw new Error(res.error);
        localStorage.setItem("token",null);
        setToken(null);
        window.location = '/login';
    })
    .catch((er) => {
        console.error("error : "+er.message);
    });
  }
  
  return (
    <div className="modal">
      <div onClick={handleClose} className='overlay'></div>
      <div className="modal-content">
        <button className='close-modal' onClick={handleClose}>close</button>
        <div>
          <img width="100" height="100" src="https://ionicframework.com/docs/img/demos/thumbnail.svg" alt="thumbnail"/>
          <h2>{currentEnchere.nomProduit}</h2>
        </div>
        <p>Categorie : {currentEnchere.categorie}</p>
        <p>Description : {currentEnchere.description}</p>
        <p>Prix Actuel : {currentEnchere.montant}Ar</p>
        <p>Client : {currentEnchere.client}</p>
        <p>Timer : {currentEnchere.dateFin}</p>
        <form onSubmit={submitForm}>
          <div>
            <button type='submit'>Encherir</button>
            <input type="number" step="0.01" value={inputValue} onChange={handleInputChange} />
          </div>
        </form>
      </div>
    </div>
  );
}

export default Fiche;
