const EnchereList = ({ encheres, handleClick }) => (
    <ul>
        {encheres.map(e => (
        <li key={e.id} onClick={() => handleClick(e)}>
            {e.nomProduit}
        </li>
        ))}
    </ul>
);

export default EnchereList;